var sensor = require('node-dht-sensor');
var sensor_type = 22;
var gpio = 18;

var signal = sensor.read(sensor_type, gpio);

sensor.getTemperature = function() {
  return signal.temperature.toFixed(1);
}

sensor.getHumidity = function() {
  return signal.humidity.toFixed(1);
}

module.exports = sensor;
