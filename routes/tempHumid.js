var express = require('express');
var router = express.Router();
//var ws = require('express-ws')(router);
//const wss = new ws.Server();
/* GET page with temperature and humidity measurements. */
router.get('/', function(req, res, next) {
  res.render('tempHumid', { page: 'Temperature_Humidity', menuId: 'tempHumid'});
});

//router.ws('/', function(s, req) {
  //console.log('websocket connection');
  //for (var t = 0; t < 20; t++){
    //ws.on('message', function(number) {
//      s.send(t);
    //});
  //}
//});
module.exports = router;
